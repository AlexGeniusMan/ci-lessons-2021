from converter import convert


def test_cel2kel():
    assert convert(25, 'cel', 'kel') == 298


def test_kel2cel():
    assert convert(25, 'kel', 'cel') == -248


def test_cel2fah():
    assert convert(25, 'cel', 'fah') == 77


def test_fah2cel():
    assert convert(25, 'fah', 'cel') == -3


def test_kel2fah():
    assert convert(25, 'kel', 'fah') == -414


def test_fah2kel():
    assert convert(25, 'fah', 'kel') == 270


def test_error_value():
    assert convert('test', 'fah', 'kel') == 'error'


def test_error_first_scale():
    assert convert(25, 'awdjbwad', 'kel') == 'error'


def test_error_second_scale():
    assert convert(25, 'fah', 'awidjwia') == 'error'
