from sys import argv


def fah2cel(fah):
    cel = 5.0 * (fah - 32) / 9
    return int(cel)


def cel2fah(cel):
    fah = (9 / 5.0 * cel) + 32
    return int(fah)


def cel2kel(cel):
    kel = cel + 273
    return int(kel)


def kel2cel(kel):
    cel = kel - 273
    return int(cel)


def fah2kel(fah):
    cel = fah2cel(fah)
    kel = cel2kel(cel)
    return int(kel)


def kel2fah(kel):
    cel = kel2cel(kel)
    fah = cel2fah(cel)
    return int(fah)


def convert(value, first_scale, second_scale):
    with open('test.txt', 'a') as the_file:
        if not isinstance(value, (int, float)):
            the_file.write(f'{value} is not a number!\n')
            return 'error'
        # print(first_scale)
        if first_scale != 'cel' and first_scale != 'kel' and first_scale != 'fah':
            the_file.write(f'Wrong first scale!\n')
            return 'error'
        if second_scale != 'cel' and second_scale != 'kel' and second_scale != 'fah':
            the_file.write(f'Wrong second scale!\n')
            return 'error'

        if first_scale == 'cel':
            if second_scale == 'fah':
                result = cel2fah(value)
            if second_scale == 'kel':
                result = cel2kel(value)
        if first_scale == 'fah':
            if second_scale == 'cel':
                result = fah2cel(value)
            if second_scale == 'kel':
                result = fah2kel(value)
        if first_scale == 'kel':
            if second_scale == 'fah':
                result = kel2fah(value)
            if second_scale == 'cel':
                result = kel2cel(value)

        the_file.write(f'{value} {first_scale} = {result} {second_scale}\n')

    return result
    # return 0

# if __name__ == '__main__':
#     value, first_scale, second_scale = argv
#     print(value, first_scale, second_scale)
#     convert(value, first_scale, second_scale)
# return
# print(convert(25, 'fah', 'kel'))
